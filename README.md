# Overlay
#### Autor: Jorge Cabrejas Peñuelas

## Introducción
Este proyecto llamado overlay intenta aislar de manera manual los entornos de ejecución de un conjunto de contenedores. 
Ello se consigue con la separación de espacios de nombres (*namespaces*), de forma que cada contenedor tiene su
propio espacio de nombres, disminuyendo la vulnerabilidad del sistema frente a posibles ataques intencionados. Este
documento se centra en el espacio de nombres de red.

## Objetivo
El objetivo de este proyecto es resolver los problemas propuestos para trabajar con espacios de nombres y usar técnicas 
basadas en el filtrado de paquetes de red mediante el mecanismo conocido como netfilter y que es implementado con 
IPTABLES o NFTABLES.

## Requisitos
Para poder llevar a cabo el despliegue que se plantea en el siguiente apartado es necesario ejecutar las siguientes 
tareas:
* Instalar python3: *apt-get install python3*.
* Hacer login en Docker Hub.
* Instalar Docker (https://docs.docker.com/engine/install/).

## Descripción de la solución
La solución planteada se basa en el despliegue de dos hosts diferentes que se encuentran en la misma red de área 
local y que cada uno puede tener un número diferente de contenedores que es configurable como parámetro. La siguiente 
figura muestra un ejemplo de despliegue que cubre todos los ejercicios planteados. En concreto, se tienen dos máquinas 
virtuales bajo dos hosts diferentes, HOST 1 y HOST 2. Note que a lo largo del presente documento se utiliza el 
término host origen para referirse al host donde se ejecuta el código Python y al host destino al otro host con el que 
se comunica. Cada una de las máquinas virtuales contienen dos contenedores Docker creados con imágenes basadas en 
Ubuntu.

![alt text](figures/overlay.png "Despliegue implementado")

Cada uno de los hosts dispone de un código Python que configura un despliegue que podría ser el de la figura anterior.
La entrada a cada proyecto se encuentra en la ruta conf/file.config y cuya descripción y ejemplo se muestra a 
continuación:

| Entradas | Descripción | Ejemplo |
| ---      |  ------  |----------|
| debug_subprocess   | Variable utilizada para el modo debug   | true   |
| pass | Password del superusuario del host donde se quiere ejecutar el programa | 12345 |
| number_of_containers_origin   | Número de contenedores del host origen | 2   |
| number_of_containers_dest   | Número de contenedores del host destino  | 3   |
| 3rd_octet_ip_origin   | Número que se introduce en el tercer octeto de la dirección IP de los contenedores del host origen | 15   |
| 3rd_octet_ip_dest   | Número que se introduce en el tercer octeto de la dirección IP de los contenedores del host destino | 30   |
| containers_origin_subnet   | Dirección IP de la subred de los contenedores  | 192.168.15.0   |
| mask   | Es la máscara de las direcciones IP que se tienen en el despliegue        | /24   |
| outward_interface   | Interfaz del host origen       | enp0s3   |
| ip_outward_interface_dest   | Dirección IP de la interfaz del host destino        | 192.168.0.30   |
| host_subnet   | Subred del host destino        | 192.168.0.0   |
| containers_dest_subnet   | Subred de los contenedores destino        | 192.168.30.0   |
| ip_bridge   | Dirección IP del bridge        | 192.168.15.254   |
| bridge_name   | Nombre del bridge        | br0   |
| step   | Paso a ejecutar        | 1   |

Note que la dirección IP de los contenedores puede ir desde 192.168.x.1 a 192.168.x.253, siendo 
x = number_of_containers_origin o x = number_of_containers_dest. Por su parte, la dirección IP del bridge se considera 
fija con valor 192.168.x.254.

El script Python debe ser ejecutado en tres pasos:
* **Paso 1**. Creación del despliegue.
* **Paso 2**. Pruebas de los ejerciciós planteados.
* **Paso 3**. Borrado del despliegue.

Dado que para completar algunos ejercicios es necesario que los hosts se comuniquen entre sí, se debería primero 
ejecutar el primer paso en cada host. Antes de ello, es necesario cambiar el parámetro de configuración "step" a 1 en 
conf/file.config. A continuación, ya se podrían ejecutar los pasos 2 o 3 (*python3 overlay.sh*).
 
El script de Python realiza las siguientes tareas:

**Preconfiguración**
* Se activa el network forwarding del host.

    ```python
    activate_routing_forwarding()
    ```

**Despliegue**
* Se crea el bridge.

    ```python
    create_bridge(config_file['bridge_name'])    
    ```

* Se asigna una dirección IP al bridge.

    ```python
    add_ip_address_to_bridge(config_file['ip_bridge'] + config_file['mask'], 'br0')
    ```
    
* Se levanta la interfaz del bridge

    ```python
    link_up_network_element_host(config_file['bridge_name'])    
    ```
    
* Se añaden reglas de IPTABLES para poder salir a Internet.

    ```python
    create_iptables_rules()
    ```
    
* Se añaden rutas IP de salto al otro host.

    ```python
    add_route_via_dev_in_host(config_file['host_subnet'] + config_file['mask'], config_file['outward_interface'])
    add_route_via_ip_in_host(
        config_file['containers_dest_subnet'] + config_file['mask'], config_file['ip_outward_interface_dest']
    )    
   ```
    
* En el bucle for se realizan las siguientes operaciones:

    1. Se crea un número configurable de imágenes correspodientes a los contenedores a crear, es decir, una imagen por 
    contenedor.
    2. Se crean los contenedores y se hacen visibles en el host. 
    3. Para cada contenedor se crea una interfaz (veth0) y otra en el host y su enlace correspondiente. 
    4. Se enlazan las interfaces del host al bridge. 
    5. Se asigna una dirección IP a la interfaz creada en cada uno de los contenedores. 
    6. Se levantan todas las interfaces de los contenedores y del host. 
    7. Se añade una ruta por defecto a los contenedores con salto al bridge.

    ```python
    for n in range(1, N + 1):
        str_n = str(n)
        # Creating the network containers
        build_image('image' + str_n)
        container_up('container' + str_n, 'image' + str_n)
        make_visible_container_as_namespaces('container' + str_n)

        # Creating network elements and connecting them each other
        link_container_to_host_with_veth('container' + str_n, 'veth0', 'veth' + str_n)
        link_veths_to_bridge('br0', 'veth' + str_n)

        # Assigning IP addresses
        add_ip_address_to_veth_container(
            '192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_n, 'container' + str_n, 'veth0'
        )

        # Starting up interfaces
        link_up_veth_container('container' + str_n, 'veth0')
        link_up_network_element_host('veth' + str_n)

        # Configuring containers
        add_route_via_dev_in_container(
            'container' + str_n, config_file['containers_origin_subnet'] + config_file['mask'], 'veth0'
        )
        add_default_route_via_ip_in_container('container' + str_n, config_file['ip_bridge'])
    ```
    
**Pruebas**
* Se realizan una serie de pings para cubrir todos los casos demandados y son mostrados en el apartado de 
resultados.

    ```python
    # Getting the ip address of the outward interface of the local host
    ip_outward_interface_origin = get_ip_outward_interface(config_file['outward_interface'])
    # Show the result of the proposed problems
    for n in range(1, n_origin + 1):

        # Problem 1
        str_n = str(n)
        for j in range(1, n_origin + 1):
            if n != j:
                str_j = str(j)
                ping_from_container_to_address(
                    'container' + str_n, '192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_j
                )

        # Problem 2
        ping_from_container_to_address('container' + str_n, ip_outward_interface_origin)

        # Problem 2a
        ping_from_host_to_address('192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_n)

        # Problem 3
        ping_from_container_to_address('container' + str_n, '8.8.8.8')

        # Problem 4
        for j in range(1, n_dest + 1):
            str_j = str(j)
            ping_from_container_to_address(
                'container' + str_n, '192.168.' + str(config_file['3rd_octet_ip_dest']) + '.' + str_j
            )

    # Problem 3a
    for n in range(1, n_dest + 1):
        str_n = str(n)
        ping_from_host_to_address('192.168.' + str(config_file['3rd_octet_ip_dest']) + '.' + str_n)
    ```
  
**Borrado**
* Se limpia el despliegue parando los contenedores, eliminándolos, eliminando las imágenes, borrando el bridge, borrando
las reglas de IPTABLES y las reglas de IPROUTE del host creadas.

    ```python
    # Removing the deployment
    stop_containers()
    remove_containers()
    remove_images()
    remove_bridge(config_file['bridge_name'])
    remove_iptables_rules()
    remove_ip_routes_in_host(config_file['host_subnet'] + config_file['mask'])
    remove_ip_routes_in_host(config_file['containers_dest_subnet'] + config_file['mask']) 
    ```
  
## Reglas de IPTABLES e IPROUTE

En este apartado se quieren destacar las reglas que se han introducido para enrutar los paquetes:

**Reglas de IPTABLES**

Las reglas que se muestran a continuación han servido para enrutar los paquetes desde cualquier contenedor hacia nodos 
accesibles desde el host:

```python
def create_iptables_rules():
    """
    This function is to create the needed rules to route the packets and complete the proposed problems
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S iptables -A FORWARD -j ACCEPT'
    execute_command(command)

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S iptables -t nat -A POSTROUTING -s ' +
            config_file['containers_origin_subnet'] + config_file['mask'] + ' -j MASQUERADE'
    )
    execute_command(command)
```

**Reglas de IPROUTE**

Las siguientes reglas han servido para enrutar paquetes de un host a otro:

```python
def add_route_via_dev_in_host(host_subnet, outward_interface):
    """
    This function is to add a route via a device to jump to another host

    :return: None
    
    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip route add ' + host_subnet + ' dev ' + outward_interface
    execute_command(command)
```

```python
def add_route_via_ip_in_host(containers_dest_subnet, ip_outward_interface_dest):
    """
    This function is to add a route via an ip address in host to jump to another host

    :return: None

    """

    command = (
        'echo ' + config_file['pass'] + ' | sudo -S ip route add ' + containers_dest_subnet + ' via ' +
        ip_outward_interface_dest
    )
    execute_command(command)
```

Además, cada contenedor debería tener una regla de IPROUTE para poder saltar al bridge como ruta por defecto.

```python
def add_default_route_via_ip_in_container(container, ip_bridge):
    """
    This function is to add a default route at the container to the bridge

    :param container: name of the container
    :param ip_bridge: ip of the bridge
    :return: None

    """
    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ip route add default via ' +
            ip_bridge
    )
    execute_command(command)
```

Es importante destacar que previamente a esta implementación, se trabajó en una solución simplificada sin bridge, pero 
no se obtuvieron buenos resultados posiblement debido a las reglas de IPTABLES que el propio Docker instala en el host.

## Resultados
En este apartado se van a mostrar los resultados de los ejercios planteados:

**Problema 1**. Los contenedores puedan comunicar entre sí mediante protocolos IP

Como se muestra en la siguiente figura se manda un ping desde la IP 192.168.15.1 a la IP 192.168.15.2 del mismo host y 
se recibe una respuesta.

![alt text](figures/overlay1.png "Esquema ejercicio 1")

El resultado es el siguiente:

![alt text](figures/ejercicio1.png "Ejercicio 1")

**Problema 2**. Los contenedores puedan comunicar con el HOST

En este ejercicio el contenedor 1 con IP 192.168.15.1 manda un ping a la interfaz enp0s3 con IP 192.168.0.25.

![alt text](figures/overlay2.png "Esquema ejercicio 2")

El resultado es el siguiente:

![alt text](figures/ejercicio2.png "Ejercicio 2")

**Problema 2a**. Desde el HOST se pueda comunicar con los contenedores

Ahora el HOST 1 envía un ping a la IP 192.168.15.1.

![alt text](figures/overlay2a.png "Esquema ejercicio 2a")

El resultado es el siguiente:

![alt text](figures/ejercicio2a.png "Ejercicio 2a")


**Problema 3**. Los contenedores puedan comunicar con cualquier nodo alcanzable desde el host

En esta ocasión la prueba que se ha realizado es mandar un ping a la dirección IP 8.8.8.8 desde el contenedor 1 del 
HOST 1.

![alt text](figures/overlay3.png "Esquema ejercicio 3")

El resultado es el siguiente:

![alt text](figures/ejercicio3.png "Ejercicio 3")

**Problema 3a**. Desde nodos que puedan alcanzar al host se pueda establecer algún tipo de comunicación con un 
contenedor

En este caso el HOST 1 envía un ping al contenedor 1 del HOST 2

![alt text](figures/overlay3a.png "Esquema ejercicio 3a")

El resultado es el siguiente:

![alt text](figures/ejercicio3a.png "Ejercicio 3a")

**Problema 4**. Si los contenedores son desplegados en nodos diferentes, éstos puedan aún comunicarse entre sí como si 
estuvieran en el mismo segmento de red (protocolo IP)

En esta prueba se envía un ping desde el contenedor 1 del HOST 1 al contenedor 1 del HOST 2.

![alt text](figures/overlay4.png "Esquema ejercicio 4")

El resultado es el siguiente:

![alt text](figures/ejercicio4.png "Ejercicio 4")

En este punto es importante destacar que aunque la solución implementada obviamente no está lista para producción, sí
que se ha optado por generalizar lo más posible. Aun así, si hubiera un mayor número de hosts, la cantidad de reglas de
IPTABLES (para enrutar hacia nodos que son accesibles desde el host) y de IPROUTE (para enrutar paquetes de un host a
otro) serían proporcionales al número de hosts. Sin embargo, es cierto que cada contenedor debería de disponer de una
regla de encaminamiento para saltar al bridge debido a la problemática de tener instalado Docker en el sistema. 