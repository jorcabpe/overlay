import json

import subprocess as sp
from subprocess import PIPE
from subprocess import STDOUT

# Reading the configuration file
with open('conf/file.config') as f:
    config_file = json.load(f)


def execute_command(command):
    """
    This function is to execute commands

    :param command: command to execute
    :return: None

    """
    result = sp.run(command, stdout=PIPE, stderr=STDOUT, shell=True)

    if config_file['debug_subprocess']:
        print(f'stdout: {result.stdout.decode("utf-8").rstrip()}')
        print(f'stderr: {result.stdout.decode("utf-8").rstrip()}')


def build_image(tag):
    """
    This function is to build an image with tag

    :param tag: tag of the image
    :return: None

    """

    command = 'docker build -t ' + tag + ' ./build'
    execute_command(command)


def container_up(container, image):
    """
    This function is to set up a container

    :param container: name of the container
    :param image: image of the container
    :return: None

    """

    # The containers initially do not have network to connect
    command = 'docker run -d --cap-add=NET_ADMIN --name ' + container + ' --net none ' + image
    execute_command(command)


def make_visible_container_as_namespaces(container):
    """
    This function is to add the container visible from host

    :param container: name of the container
    :return: None

    """

    # Creating the folder netns if it does not exist
    command = 'echo ' + config_file['pass'] + ' | sudo -S mkdir -p /var/run/netns'
    execute_command(command)

    command = 'docker inspect ' + container + ' -f {{.State.Pid}}'
    out = sp.check_output(command, shell=True)
    pid = out.decode("utf-8").split('\n')[0]

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ln -sf /proc/' + str(pid) + '/ns/net /var/run/netns/' +
            container
    )
    execute_command(command)


def link_up_veth_container(container, veth):
    """
    This function is to up the veth of the containeri

    :param container: name of the container
    :param veth: name of the veth
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ip link set ' + veth + ' up'
    execute_command(command)


def add_ip_address_to_veth_container(ip_with_mask, container, veth):
    """
    This function is to assign an IP address to the veth of the container

    :param ip_with_mask: IP address to assign
    :param container: name of the container
    :param veth: name of the veth
    :return: None

    """

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ip addr add ' + ip_with_mask +
            ' dev ' + veth
    )
    execute_command(command)


def ping_from_container_to_address(container, ip):
    """
    This function is to sent a ping packet from the container to the address ip

    :param container: name of the container
    :param ip: destination IP adddress
    :return: result of the ping

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ping -c 3 ' + ip

    result = sp.run(command, stdout=PIPE, stderr=STDOUT, shell=True)

    print(f'stdout: {result.stdout.decode("utf-8").rstrip()}')

    if config_file['debug_subprocess']:
        print(f'stderr: {result.stdout.decode("utf-8").rstrip()}')


def stop_containers():
    """
    This function is to stop all containers

    :return:None

    """

    command = 'docker stop $(docker ps -aq)'
    execute_command(command)


def remove_containers():
    """
    This function is to remove all containers

    :return: None

    """

    command = 'docker rm $(docker ps -aq)'
    execute_command(command)


def remove_images():
    """
    This function is to remove all images

    :return: None

    """

    command = 'docker rmi $(docker images -q) --force'
    execute_command(command)


def link_container_to_host_with_veth(container, veth_container, veth_host):
    """
    This function is to add a link between the veth in the container and the host

    :param container: name of the container
    :param veth_container: name of the veth of the container
    :param veth_host: name of the veth of the host
    :return: None

    """

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ip link add ' + veth_container + ' netns ' + container +
            ' type veth peer name ' + veth_host
    )
    execute_command(command)


def link_up_network_element_host(destination):
    """
    This function is to up the network element destination

    :param destination: network element to up
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip link set ' + destination + ' up'
    execute_command(command)


def ping_from_host_to_address(ip):
    """
    This function is to sent a ping from the host to the address ip (container)

    :param ip: destination IP adddress of the ping
    :return: result of the ping

    """

    command = 'ping -c 3 ' + ip

    result = sp.run(command, stdout=PIPE, stderr=STDOUT, shell=True)

    print(f'stdout: {result.stdout.decode("utf-8").rstrip()}')

    if config_file['debug_subprocess']:
        print(f'stderr: {result.stdout.decode("utf-8").rstrip()}')


def create_bridge(bridge):
    """
    This function is to create the bridge

    :param bridge: name of the bridge to create
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S apt-get install bridge-utils'
    execute_command(command)

    command = 'echo ' + config_file['pass'] + ' | sudo -S brctl addbr ' + bridge
    execute_command(command)


def link_veths_to_bridge(bridge, veth):
    """
    This function is to add veths to the bridge

    :param bridge: name of the bridge
    :param veth: veth to allocate in the bridge
    :return: None

    """
    command = 'echo ' + config_file['pass'] + ' | sudo -S brctl addif ' + bridge + ' ' + veth
    execute_command(command)


def add_ip_address_to_bridge(ip_with_mask, bridge):
    """
    This function is to assign an IP address to the bridge

    :param ip_with_mask: IP address to assign
    :param bridge: name of the bridge
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip addr add ' + ip_with_mask + ' dev ' + bridge
    execute_command(command)


def remove_bridge(bridge):
    """
    This function is to remove the bridge

    :param bridge: name of the bridge
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S apt-get install net-tools'
    execute_command(command)

    command = 'echo ' + config_file['pass'] + ' | sudo -S ifconfig ' + bridge + ' down'
    execute_command(command)

    command = 'echo ' + config_file['pass'] + ' | sudo -S brctl delbr ' + bridge
    execute_command(command)


def create_iptables_rules():
    """
    This function is to create the needed rules to route the packets and complete the proposed problems
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S iptables -A FORWARD -j ACCEPT'
    execute_command(command)

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S iptables -t nat -A POSTROUTING -s ' +
            config_file['containers_origin_subnet'] + config_file['mask'] + ' -j MASQUERADE'
    )
    execute_command(command)


def remove_iptables_rules():
    """
    This function is to remove the added rules above and clean the setup
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S iptables -D FORWARD -j ACCEPT'
    execute_command(command)

    command = (
            'echo ' + config_file['pass'] + ' | sudo -S iptables -t nat -D POSTROUTING -s ' +
            config_file['containers_origin_subnet'] + config_file['mask'] + ' -j MASQUERADE'
    )
    execute_command(command)


def add_default_route_via_ip_in_container(container, ip_bridge):
    """
    This function is to add a default route at the container to the bridge

    :param container: name of the container
    :param ip_bridge: ip of the bridge
    :return: None

    """
    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ip route add default via ' +
            ip_bridge
    )
    execute_command(command)


def add_route_via_dev_in_container(container, ip_bridge_w_mask, interface):
    """
    This function is to add a default route at the container to the bridge

    :param container: name of the container
    :param ip_bridge_w_mask: ip of the bridge
    :return: None

    """
    command = (
            'echo ' + config_file['pass'] + ' | sudo -S ip netns exec ' + container + ' ip route add ' +
            ip_bridge_w_mask + ' dev ' + interface
    )
    execute_command(command)


def add_route_via_ip_in_host(containers_dest_subnet, ip_outward_interface_dest):
    """
    This function is to add a route via an ip address in host to jump to another host

    :return: None

    """

    command = (
        'echo ' + config_file['pass'] + ' | sudo -S ip route add ' + containers_dest_subnet + ' via ' +
        ip_outward_interface_dest
    )
    execute_command(command)


def add_route_via_dev_in_host(host_subnet, outward_interface):
    """
    This function is to add a route via a device to jump to another host

    :return: None
    
    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip route add ' + host_subnet + ' dev ' + outward_interface
    execute_command(command)


def remove_ip_routes_in_host(subnet):
    """
    This function is to remove the ip routes created in the host

    :param subnet: destination subnet name
    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S ip route del ' + subnet
    execute_command(command)


def activate_routing_forwarding():
    """
    This function is to activate the routing forwarding

    :return: None

    """

    command = 'echo ' + config_file['pass'] + ' | sudo -S bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"'
    execute_command(command)


def get_ip_outward_interface(interface):

    """
    This function is to obtain the outward interface of the local host

    :param interface: the name of the interface
    :return: None

    """
    command = (
        'ip addr show dev ' + interface +
        ' | grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*"'
    )

    result = sp.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
    ip = result.stdout.decode("utf-8").rstrip()

    return ip


def get_deployment(N):

    """
    This function is to build the deployment under study

    :param N: number of containers by virtual machine
    :return: None

    """

    # Activating network routing
    activate_routing_forwarding()

    # Creating the bridge
    create_bridge(config_file['bridge_name'])
    add_ip_address_to_bridge(config_file['ip_bridge'] + config_file['mask'], 'br0')
    link_up_network_element_host(config_file['bridge_name'])

    # Adding the iptables and iproute rules
    create_iptables_rules()
    add_route_via_dev_in_host(config_file['host_subnet'] + config_file['mask'], config_file['outward_interface'])
    add_route_via_ip_in_host(
        config_file['containers_dest_subnet'] + config_file['mask'], config_file['ip_outward_interface_dest']
    )

    for n in range(1, N + 1):
        str_n = str(n)
        # Creating the network containers
        build_image('image' + str_n)
        container_up('container' + str_n, 'image' + str_n)
        make_visible_container_as_namespaces('container' + str_n)

        # Creating network elements and connecting them each other
        link_container_to_host_with_veth('container' + str_n, 'veth0', 'veth' + str_n)
        link_veths_to_bridge('br0', 'veth' + str_n)

        # Assigning IP addresses
        add_ip_address_to_veth_container(
            '192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_n + config_file['mask'], 'container'
            + str_n, 'veth0'
        )

        # Starting up interfaces
        link_up_veth_container('container' + str_n, 'veth0')
        link_up_network_element_host('veth' + str_n)

        add_default_route_via_ip_in_container('container' + str_n, config_file['ip_bridge'])


def main():

    # step:
    #      1: Get the deployment
    #      2: Testing
    #      3: Remove the deployment

    # Getting some configuration
    n_origin = config_file['number_of_containers_origin']
    n_dest = config_file['number_of_containers_dest']

    if config_file['step'] == 1:
        # Getting the deployment
        get_deployment(n_origin)

    if config_file['step'] == 2:
        # Getting the ip address of the outward interface of the local host
        ip_outward_interface_origin = get_ip_outward_interface(config_file['outward_interface'])
        # Show the result of the proposed problems
        for n in range(1, n_origin + 1):

            # Problem 1
            str_n = str(n)
            for j in range(1, n_origin + 1):
                if n != j:
                    str_j = str(j)
                    ping_from_container_to_address(
                        'container' + str_n, '192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_j
                    )

            # Problem 2
            ping_from_container_to_address('container' + str_n, ip_outward_interface_origin)

            # Problem 2a
            ping_from_host_to_address('192.168.' + str(config_file['3rd_octet_ip_origin']) + '.' + str_n)

            # Problem 3
            ping_from_container_to_address('container' + str_n, '8.8.8.8')

            # Problem 4
            for j in range(1, n_dest + 1):
                str_j = str(j)
                ping_from_container_to_address(
                    'container' + str_n, '192.168.' + str(config_file['3rd_octet_ip_dest']) + '.' + str_j
                )

        # Problem 3a
        for n in range(1, n_dest + 1):
            str_n = str(n)
            ping_from_host_to_address('192.168.' + str(config_file['3rd_octet_ip_dest']) + '.' + str_n)

    if config_file['step'] == 3:
        # Removing the deployment
        stop_containers()
        remove_containers()
        remove_images()
        remove_bridge(config_file['bridge_name'])
        remove_iptables_rules()
        remove_ip_routes_in_host(config_file['host_subnet'] + config_file['mask'])
        remove_ip_routes_in_host(config_file['containers_dest_subnet'] + config_file['mask'])


if __name__ == "__main__":
    main()
